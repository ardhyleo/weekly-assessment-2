// write function here
const fizzBuzz = (data) => {
  for (let i = 0; i < data; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
      console.log("Fizzbuzz");
    } else if (i % 3 == 0) {
      console.log("Fizz");
    } else if (i % 5 == 0) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
};

// input test
const input1 = 16;
const input2 = 100;

fizzBuzz(input1);
fizzBuzz(input2);
