// write function here
const oddEven = (data) => {
  if (data % 2 == 0) {
    console.log(`${data} Even`);
  } else {
    console.log(`${data} Odd`);
  }
};

// input test
const input1 = 3;
const input2 = 10;
const input3 = 5;

oddEven(input1); // output: 3 is odd number
oddEven(input2);
oddEven(input3);
