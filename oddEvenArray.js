// write function here
const oddEvenArray = (data) => {
  for (let i = 0; i < data.length; i++) {
    if (data[i] % 2 == 0) {
      console.log(`${data[i]} Even Number`);
    } else {
      console.log(`${data[i]} Odd Number`);
    }
  }
};

// input test
const input1 = [1, 4, 6, 3, 10, 7];

oddEvenArray(input1);
