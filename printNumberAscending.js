// write function here
const printNumberAscending = (data) => {
  for (let i = 0; i <= data; i++) {
    console.log(i);
  }
};

// input test
const input1 = 5;
const input2 = 10;

printNumberAscending(input1);
printNumberAscending(input2); // output: 0 1 2 3 4 5
