# OH WEEKLY ASSESSMENT 2

In this assessment, you have to answer some basic question about Javascript and solve some problems using Javascript. You can find the answer from the internet or by discussing with your friend, but make sure you understand what you are doing. Please deliver this assessment on time no matter what results you make! Thank you.

**Deadline**: Friday, 22 October 2021, 23:59 WIB

## Instructions

- Clone this project repository
- Try to complete complete the task
- Create git repository using `weekly-assessment-2` as the name of project
- Push your result to it
- Fill this form when you are finish: https://forms.gle/NLSnsuddjz1BPzeW6

## Task Requirements

- [ ] Complete Task 1: Understanding Javascript
- [ ] Complete Task 2: printNumberAscending.js
- [ ] Complete Task 3: printNumberDescending.js
- [ ] Complete Task 4: oddEven.js
- [ ] Complete Task 5: oddEvenArray.js
- [ ] Complete Task 6: fizzBuzz.js
- [ ] Push your result in your git repository

Extra miles 🚀🚀🚀

- [ ] Complete Task 7: sumArray.js

## Tasks

### Task 1: Understanding Javascript

> Answer the questions below in `understanding-javascript.md`. You can answer using bahasa or english.

1. What is the difference between **Javascript** and **HTML**?
2. What is the differenece between `var`, `let`, and `const`?
3. What **data types** in javascript do you know?
4. What do you know about `function`?
5. What do you know about **hoisting**?

### Task 2: printNumberAscending.js

> Answer the questions below in `printNumberAscending.js`. If you can't create the function, describe what your solution is step by step.

Create a function that will print numbers in ascending order!

```bash
Example
Input: 5
Output:
0 1 2 3 4 5
```

### Task 3: prinNumberDescending.js

> Answer the questions below in `printNumberDescending.js`. If you can't create the function, describe what your solution is step by step.

Create a function that will print numbers in descending order!

```bash
Example
Input: 5
Output:
5 4 3 2 1
```

### Task 4: oddEven.js

> Answer the questions below in `oddEven.js`. If you can't create the function, describe what your solution is step by step.

Create a function that will determined input number is even or odd number!

```bash
Example 1
Input: 3
Output:
3 is odd number

Example 2
Input: 10
Output:
10 is even number

Example 3
Input: 5
Output:
5 is odd number
```

### Task 5: oddEvenArray.js

> Answer the questions below in `oddEvenArray.js`. If you can't create the function, describe what your solution is step by step.

Create a function that will determined input array is have even or odd number!

```bash
Example 1
Input: [1, 4, 6, 3, 10, 7]
Output:
1 is odd number
4 is even number
6 is even number
3 is odd number
10 is even number
7 is odd number
```

### Task 6: fizzBuzz.js

> Answer the questions below in `fizzBuzz.js`. If you can't create the function, describe what your solution is step by step.

Create a function that will print numbers with specific condition:

1. Start from number 1
2. If number can be divided by 3 change number to `Fizz`
3. If number can be divided by 5 change number to `Buzz`
4. If number can be divided by 3 and 5 change number to `FizzBuzz`

```bash
Example 1
Input: 16
Output:
1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16
```

### Task 7: sumArray.js

> Answer the questions below in `sumArray.js`. If you can't create the function, describe what your solution is step by step.

Create a function that will sum all numbers in an array!

```bash
Example 1
Input: [1, 2, 5, 8, 9, 10]
Output: 35

Example 2
Input: [1, 2, 3, 4, 5]
Output: 15
```

## References

- [Javascript Fundamentals](https://javascript.info/first-steps)
