# Task 1: Understanding Javascript

1. What is the difference between **Javascript** and **HTML**?
   perbedaannya terletak di penerapannya di website, HTML menjelaskan bagaimana menerapkan tampilan halaman website, sedangkan javascript menjelaskan bagaimana berinteraksi dengan halaman web.
2. What is the differenece between `var`, `let`, and `const`?
   perbedaan sederhananya terletak di objeknya, var merupakan tipe variabel global, let sama dengan var tapi let bisa digunakan di variabel block, sedangkan const adalah tipe variabel block.
3. What **data types** in javascript do you know?
   Tipe Data adalah sebuah pengklasifikasian data berdasarkan jenis data tersebut. Tipe data dibutuhkan agar kompiler dapat mengetahui bagaimana sebuah data akan digunakan. ex, data string, boolean, number, dll
4. What do you know about `function`?
   Function adalah suatu program terpisah dalam blok sendiri yang berfungsi sebagai sub-program (modul program) yang merupakan sebuah program kecil untuk memproses sebagian dari pekerjaan program utama.
5. What do you know about **hoisting**?
   Suatu kompilator JavaScript memindahkan semua deklarasi variabel dan fungsi ke atas sehingga tidak akan ada kesalahan apa pun.
